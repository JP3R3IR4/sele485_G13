#include <Arduino.h>
#include <avr/io.h>

#define ON 255  // b11111111
#define OFF 127  // b0000000
 
#define FOSC 16000000 // Clock Speed #define BAUD 9600
#define BAUD 9600
#define UBRR FOSC/16/BAUD-1

uint8_t AdressSlave1, AdressSlave2, data, adress_read, adress, state;

void USART_Init(unsigned int ubrr) {
    /*Set baud rate */
    UBRR0H = (unsigned char)(ubrr>>8); 
    UBRR0L = (unsigned char) ubrr;
    /* Enable receiver and transmitter */ 
    UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<UCSZ02);
    /* Set frame format: 9 data, 1 stop bit. odd parity bit*/ 
    UCSR0C = (1<<UCSZ00)|(1<<UCSZ01)|(1<<UPM01)|(1<<UPM00);
}

void USART_TransmitAdress(uint8_t adress) {
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)));
    /* Copy 9th bit to TXB8 */
    UCSR0B |= (1<<TXB80); // address
    /* Put data into buffer, sends the data */ 
    UDR0 = adress;
}

void USART_TransmitData(uint8_t data) {
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)));
    /* Copy 9th bit to TXB8 */
    UCSR0B &= ~(1<<TXB80); // data
    /* Put data into buffer, sends the data */ 
    UDR0 = data;
}

uint8_t USART_Receive(uint8_t *buffer) {
  uint8_t status, bit_nine;
  /* Wait for data to be received */ 
  while (!(UCSR0A & (1<<RXC0)));
  /* Get status and 9th bit, then data */
  /* from buffer */
  status = UCSR0A;
  bit_nine = UCSR0B;
  /* If error, return -1 */
  if (status & (((1<<FE0)|((1<<DOR0))|(1<<UPE0)))) return -1;
  /* Filter the 9th bit, then return */
  bit_nine = (bit_nine >> 1) & 0x01;
  *buffer = UDR0;
  return bit_nine;
}

void setup() {
  //USART Setup
  USART_Init(UBRR);
  //Outputs Setup
  DDRB |=  (1<<PB5); //PB5(13)
  DDRC |=  (1<<PC0); //PC0(A0)
  //Enable Inputs
  PORTB |= (1<<PB3)|(1<<PB2)|(1<<PB1)|(1<<PB0); //PB3(11), PB3(10), PB1(9), PB0(8) 
  PORTC |= (1<<PC2)|(1<<PC1); //PC3(A2), PC2(A1) 

  if((PINB & 0x0F) == 0x00) { //Master
    PORTC = 0x01;  //_RE disabled -> TX on / RX off
  }
  else if((PINB & 0x0F) == 0x01) { //Slave 1
    PORTC = 0x00;  //_RE enabled -> TX off / RX on
    UCSR0A = (1<<MPCM0);
  }
  else if((PINB & 0x0F) == 0x02) { //Slave 2
    PORTC = 0x00;  //_RE enabled -> TX off / RX on
    UCSR0A = (1<<MPCM0);
  }

}

void loop() {

  if((PINB & 0x0F) == 0x00) { //Master
    if(!(PINC & 0x02)) {
	  UCSR0B |= (1<<TXEN0);
	  adress = 0x0F;
      USART_TransmitAdress(adress);
      USART_TransmitData(ON); 
      PORTB |= (1<<PB5);
	  state |= 0x02;
	  UCSR0B |= (0<<TXEN0);
    }
    else if(!(PINC & 0x04)) {
      UCSR0B |= (1<<TXEN0);
	  adress = 0xF0;
      USART_TransmitAdress(adress);
      USART_TransmitData(ON);
      PORTB |= (1<<PB5);
	  state |= 0x01;
	  UCSR0B |= (0<<TXEN0);
    }
    else {
	  if((state & 0x01) == 0x01 && (PINC & 0x04)) { 
	    UCSR0B |= (1<<TXEN0);
	 	adress = 0xF0;
        USART_TransmitAdress(adress);
        USART_TransmitData(OFF);
	    UCSR0B |= (0<<TXEN0);
	    state &= ~(0x01);
        PORTB &= ~(1<<PB5);
      }
	  else if((state & 0x02) == 0x02 && (PINC & 0x02)) { 
	    UCSR0B |= (1<<TXEN0);
	    adress = 0x0F;
        USART_TransmitAdress(adress);
        USART_TransmitData(OFF);
	    UCSR0B |= (0<<TXEN0);
	    state &= ~(0x02);
        PORTB &= ~(1<<PB5);
      }
	}
  } 
  else if((PINB & 0x0F) == 0x01) {

	USART_Receive(&adress_read);

    if(adress_read == 0x0F) { //Receives adress, checks if matches
      UCSR0A &= ~(1<<MPCM0); 

	  USART_Receive(&data);
	  if(data == 0xFF) PORTB |= (1<<PB5);
	  else PORTB &= ~(1<<PB5);
      UCSR0A |= (1<<MPCM0); 
    }

	data = 0x00;
  }
   else if((PINB & 0x0F) == 0x02) {

	USART_Receive(&adress_read);

    if(adress_read == 0xF0) { //Receives adress, checks if matches
      UCSR0A &= ~(1<<MPCM0); 

	  USART_Receive(&data);
	  if(data == 0xFF) PORTB |= (1<<PB5);
	  else PORTB &= ~(1<<PB5);
      UCSR0A |= (1<<MPCM0); 
    }
	data = 0x00;
  }

}
