# sele485_G13 

## Contents

- [PCB](#PCB)
	- [Schematic](#Schematic)
	- [PCB Design](#PCB-Design)
- [USART Configuration](#USART-Configuration)
- [Transmission](#Transmission)
	- [Address](#Address)
	- [Data](#Data)
- [Reception](#Reception)
- [Setup](#Setup)
	- [USART Setup](#USART-Setup)
	- [Outputs Setup](#Outputs-Setup) 
	- [Enable Inputs](#Enable-Inputs)
	- [Module](#Module)
- [Loop](#Loop)
	- [Master](#Master)
	- [Slave](#Slave)

 
### PCB

#### Schematic

![scm](https://user-images.githubusercontent.com/61596101/100028650-51eb4300-2de7-11eb-9317-fb9f6f5091b4.png)

#### PCB Design

![pcb](https://user-images.githubusercontent.com/61596101/100028530-12bcf200-2de7-11eb-82b9-3722477a86a7.png)

### USART Configuration

### Initialization

Microcontroller frequency is set to 16 MHz. The serial communication has a baudrate of 9600bps, 

```
#define FOSC 16000000 
#define BAUD 9600
#define UBRR FOSC/16/BAUD-1
```
###### Note: 

To be able to use USART, the receiver (RX) and the transmitter (TX) can be enable On UCSRnB register; the frame format with the 9th bit is also enabled here.
To minimize the traffic in the serial bus, the transmiter (TX) could not be enabled.
It could just be enabled when a button is pressed.
```
UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<UCSZ02);
```

The frame format is: (9th bit +) 8 data bits, odd parity bit and 1 stop bit.
```
UCSR0C = (1<<UCSZ00)|(1<<UCSZ01)|(1<<UPM01)|(1<<UPM00);
```

### Transmission

#### Address

Function to send an Address Frame:

```
void USART_TransmitAdress(uint8_t adress) {
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)));
    /* Copy 9th bit to TXB8 */
    UCSR0B |= (1<<TXB80); // address
    /* Put data into buffer, sends the data */ 
    UDR0 = adress;
}
```

#### Data

Function to send a Data Frame:

```
void USART_TransmitData(uint8_t data) {
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)));
    /* Copy 9th bit to TXB8 */
    UCSR0B &= ~(1<<TXB80); // data
    /* Put data into buffer, sends the data */ 
    UDR0 = data;
}
```

### Reception

Function to receive data:

```
uint8_t USART_Receive(uint8_t *buffer) {
  uint8_t status, bit_nine;
  /* Wait for data to be received */ 
  while (!(UCSR0A & (1<<RXC0)));
  /* Get status and 9th bit, then data */
  /* from buffer */
  status = UCSR0A;
  bit_nine = UCSR0B;
  /* If error, return -1 */
  if (status & (((1<<FE0)|((1<<DOR0))|(1<<UPE0)))) return -1;
  /* Filter the 9th bit, then return */
  bit_nine = (bit_nine >> 1) & 0x01;
  *buffer = UDR0;
  return bit_nine;
}
```

###### Note: After checking for errors, 9th bit is filtered from UCSR0B register and it is returned. The parameter buffer is updated with new received data from *UDR0*:

### Setup

#### Usart Setup

```
void USART_Init(unsigned int ubrr) {
    /*Set baud rate */
    UBRR0H = (unsigned char)(ubrr>>8); 
    UBRR0L = (unsigned char) ubrr;
    /* Enable receiver and transmitter */ 
    UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<UCSZ02);
    /* Set frame format: 9 data, 1 stop bit. odd parity bit*/ 
    UCSR0C = (1<<UCSZ00)|(1<<UCSZ01)|(1<<UPM01)|(1<<UPM00);
}
```
#### Outputs Setup

```
  DDRB |=  (1<<PB5); //PB5(13)
  DDRC |=  (1<<PC0); //PC0(A0)

```
#### Enable Inputs

```
  PORTB |= (1<<PB3)|(1<<PB2)|(1<<PB1)|(1<<PB0); //PB3(11), PB3(10), PB1(9), PB0(8) 
  PORTC |= (1<<PC2)|(1<<PC1); //PC3(A2), PC2(A1) 
```

#### Module
Find out if it's Master or Slave1 or Slave2:

```
if((PINB & 0x0F) == 0x00) { //Master
    PORTC = 0x01;  //_RE disabled -> TX on / RX off
}
else if((PINB & 0x0F) == 0x01) { //Slave 1
    PORTC = 0x00;  //_RE enabled -> TX off / RX on
    UCSR0A = (1<<MPCM0);
}
else if((PINB & 0x0F) == 0x02) { //Slave 2
    PORTC = 0x00;  //_RE enabled -> TX off / RX on
    UCSR0A = (1<<MPCM0);
}
```
### Loop

Same logic as in Setup, checks if it is Master or Slave1 or Slave2.

#### Master:

```
if((PINB & 0x0F) == 0x00) { //Master
    if(!(PINC & 0x02)) {
      UCSR0B |= (1<<TXEN0);
      adress = 0x0F;
      USART_TransmitAdress(adress);
      USART_TransmitData(ON); 
      PORTB |= (1<<PB5);
      state |= 0x02;
      UCSR0B |= (0<<TXEN0);
    }
    ...
    else if((state & 0x02) == 0x02 && (PINC & 0x02)) { 
      UCSR0B |= (1<<TXEN0);
      adress = 0x0F;
      USART_TransmitAdress(adress);
      USART_TransmitData(OFF);
      UCSR0B |= (0<<TXEN0);
      state &= ~(0x02);
      PORTB &= ~(1<<PB5);
    }
```
###### Note: Only showing the controll of 1 slave. Slave 2 is the same.

1. Button is pressed/released. 
2. Enables USART transmiter.
3. Sends Adress frame. 
4. Sends Data frame (ON / OFF).  
5. State byte variable is 1/0 on set bit;
6. Diables USART transmiter.

###### Note: Adress frame is either 0xF0 or 0x0F because while testing 0x01 and 0x02 were not being transmited with current bit parity configuration. Experimentally 0xF0 and 0x0F work with current bit parity configuration.
###### Note: Data frames are either 0xFF(ON) and 0x7F(OFF) to be diferentiated. Could be any byte configuration.
###### Note: State byte only exists to prevent trasnmiter to be always on. This way, when button is released, the transmiter has the opportunity to send a final OFF Data frame. Then it blocks the possibility of continuously sending OFF data frames by changing the state byte.

#### Slave:

```
...
else if((PINB & 0x0F) == 0x01) {
  USART_Receive(&adress_read);
  if(adress_read == 0x0F) { //Receives adress, checks if matches
    UCSR0A &= ~(1<<MPCM0); 
    USART_Receive(&data); 
    if(data == 0xFF) PORTB |= (1<<PB5);
    else PORTB &= ~(1<<PB5);
    UCSR0A |= (1<<MPCM0); 
  }
data = 0x00;
}
```

1. Reads adress and checks if it matches
2. Disables multi-processor communication mode
3. Reads Data frame
4. Turns the led ON/OFF
6. Enables multi-processor communication mode

